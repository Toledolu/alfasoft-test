# AltaSoft Test

AlfaSoft PHP Developer recruitment test done in 2022.

## Getting Started

### Prerequisites

You will need a version of Wordpress 5 or above.

### Installing

**1:** Place the 'assets' folder in the root directory.

**2:** Place the 'persons_management-plugin' directory within the wp-content/plugins/ folder.

**3:** Copy the theme 'twentytwentytwo' in your themes fodler.

**4:** Execute the 'init.sql' script in your database to create all tables. Modify your 'wp-config.php' to properly access your database.

**5:** Go to the WP Admin Panel and select the 'plugins' section on the side menu. Then activate the 'Persons Management' plugin.

**6:** Go to Permalinks settings then change the config to show the Post Name in the URL.

**7:** Create Two Posts, the first with name/slug 'customTemplate' and using 'customTemplate' as model. And the second with name/slug 'customTemplate2' and using 'customTemplate2'.

**8:** Go to Reading Settings and change the initial Post page to use 'customTemplate' post created.

All right, now you can access the URL + '/customtemplate/' to see all Persons registered in database.

## TODO

Add Image (https://app.pixelencounter.com/api/basic/monsters/random) to Person on creation;
Improve frontend for listing Persons/Contacts;
Set customTemplate as Homepage;

## Authors

* **Lucas Toledo Heilig** - [Toledolu](https://gitlab.com/Toledolu)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
