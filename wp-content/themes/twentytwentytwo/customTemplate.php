<?php /* Template Name: customTemplate */ ?>

<?php

$sql = "SELECT id, name, email FROM wp_persons WHERE deletedAt is NULL";
$persons = $wpdb->get_results($sql);

?>

<div id="primary" class="content-area">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
  
  <main id="main" class="site-main" role="main">

  <h1>Showing All Persons</h1>
  <table id="myTable" class="display">
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($persons as $key=>$person) { ?> 
        <tr>
          <td><?php echo $person->name; ?></td>
          <td><?php echo $person->email; ?></td>
          <td><a href="/index.php/customTemplate2?id=<?php echo $person->id; ?>">Show contacts</a></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>

  </main>
</div>


<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
<script>
  $(document).ready(() => {
    $('#myTable').DataTable();
  });
</script>
