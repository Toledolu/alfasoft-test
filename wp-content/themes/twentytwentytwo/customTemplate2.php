<?php /* Template Name: customTemplate2 */ ?>

<?php
$personId = $_GET['id'];
$sql = "SELECT id, name, email FROM wp_persons WHERE id = '" . $personId . "'";
$person = $wpdb->get_results($sql)[0];
$sql = "SELECT id, countryCode, number FROM wp_contacts WHERE personId = '" . $personId . "'";
$contacts = $wpdb->get_results($sql);

?>

<div id="primary" class="content-area">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
  
  <main id="main" class="site-main" role="main">

  <h1>Showing All Contacts from: <?php echo $person->name; ?></h1>
  <table id="myTable" class="display">
    <thead>
      <tr>
        <th>Country Code</th>
        <th>Number</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($contacts as $key=>$contact) { ?> 
        <tr>
          <td><?php echo $contact->countryCode; ?></td>
          <td><?php echo $contact->number; ?></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>

  </main>
</div>


<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
<script>
  $(document).ready(() => {
    $('#myTable').DataTable();
  });
</script>
