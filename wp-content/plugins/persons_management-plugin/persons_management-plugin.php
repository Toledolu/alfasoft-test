<?php 
/*
Plugin Name: Persons Management
Description: 
Author: Lucas Toledo
Version: 1.0.0
*/
?>

<?php
// error_reporting(E_ALL);
// ini_set('display_errors', '1');

add_action('admin_menu', 'setupMenu');
// add_action('wp_head','prefix_enqueue');
// add_action('wp_enqueue_scripts','prefix_enqueue');

function setupMenu(){
  add_menu_page('Persons Management Persons List', 'Persons List', 'manage_options', 'persons_list', 'init');
  add_submenu_page('persons_list', 'Persons Management Persons Create', 'Persons Create', 'edit_themes', 'persons_list&action=create', 'init');
}
 
function init() {
  echo '<link href="/assets/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>';
  echo '<div class="container">';

  if($_GET['action'] == 'edit' && $_GET['id']) {
    return edit($_GET['id']);
  }

  if($_GET['action'] == 'delete' && $_GET['id']) {
    return delete($_GET['id']);
  }

  if($_GET['action'] == 'addContact' && $_GET['id']) {
    return addContact($_GET['id']);
  }

  if($_GET['action'] == 'deleteContact') {
    return deleteContact($_GET['contactId']);
  }

  if($_GET['action'] == 'create') {
    return create();
  }

  listAll();

  echo '</div>';
  // echo '<script src="assets/js/bootstrap.min.js"></script>';
}

function listAll() {
  global $wpdb;

  $sql = "SELECT id, name, email FROM wp_persons WHERE deletedAt is NULL";
  $persons = $wpdb->get_results($sql);

  echo '<div class="row d-flex flex-wrap align-items-center mt-3 mb-5">' . 
    '<div class="col-8">' .
      '<h1 class="mb-0">Showing all Persons</h1>' .
    '</div>' .
    '<div class="col-4">' .
      '<a href="/wp-admin/admin.php?page=persons_list&action=create" class="btn btn-primary ml-auto">Add Person</a>' .
    '</div>' .
  '</div>';

  echo '<div class="row w-100 align-items-center font-weight-bold text-center font-size-20">' . 
      '<p class="col-4">Name</p>' . 
      '<p class="col-4">Email</p>' . 
      '<p class="col-3 offset-1">Actions</p>' . 
    '</div>';

  foreach($persons as $key=>$person) {
    $url = '/wp-admin/admin.php?page=persons_list&id=' . $person->id . '&action=';

    echo '<div class="row w-100 align-items-center border-bottom pb-2 pt-2">';
    echo '<p class="col-4 mb-0">' . $person->name . '</p>';
    echo '<p class="col-4 mb-0">' . $person->email . '</p>';
    echo '<div class="offset-1 col-3 d-flex justify-content-center align-items-center">' . 
      '<a href="' . $url . 'addContact" class="btn btn-primary mr-2">Add Contact</a>' .
      '<a href="' . $url . 'edit" class="btn btn-info mr-2">Edit</a>' .
      '<a href="' . $url . 'delete" class="btn btn-danger">Delete</a>' .
    '</div>';
    echo '</div>';
  }
}

function create() {
  if($_SERVER['REQUEST_METHOD'] === 'POST') {
    return save();
  }

  echo '<div class="row d-flex flex-wrap align-items-center mt-3 mb-5">' . 
    '<div class="col-8">' .
      '<h1 class="mb-0">Creating new Person</h1>' .
    '</div>' .
    '<div class="col-4">' .
      '<a href="/wp-admin/admin.php?page=persons_list" class="btn btn-warning ml-auto">Go back</a>' .
    '</div>' .
  '</div>';

  echo '<div class="row">' .
    '<div class="col-8">' . 
      '<form method="POST" action="">' . 
        '<div class="form-group">' .
          '<label for="fld-name">Name</label>' . 
          '<input id="fld-name" name="name" type="text" class="form-control" placeholder="Person name" minlength="5" required/>' .
        '</div>' .
        '<div class="form-group">' .
          '<label for="fld-email">Email address</label>' . 
          '<input id="fld-email" name="email" type="email" class="form-control" placeholder="Person email" required/>' .
        '</div>' .
        '<input type="submit" value="Save" class="btn  btn-primary">' .
      '</form>' .
    '</div>' .
  '</div>';
}

function save() {
  if($_SERVER['REQUEST_METHOD'] === 'POST') {
    global $wpdb;

    $emailExists = checkIfExists('wp_persons', 'email', $_REQUEST['email']);

    if($emailExists) {
      echo '<div class="alert alert-danger" role="alert">Email already exists in database</div>';
      return listAll();
    }

    if($_POST['id']) {
      $wpdb->update('wp_persons', [
        'name' => $_REQUEST['name'],
        'email' => $_REQUEST['email'],
      ], [
        'id' => $_POST['id']
      ]);
    }

    $wpdb->insert('wp_persons', [
      'name' => $_REQUEST['name'],
      'email' => $_REQUEST['email'],
    ]);

    // return header("Location: /wp-admin/admin.php?page=persons_list", TRUE, 301);
    listAll();
  }
}

function checkIfExists($table, $column, $value) {
  global $wpdb;

  return $wpdb->get_var("SELECT id FROM $table WHERE $column = '$value' AND deletedAt IS NULL");
}

function edit($id) {
  global $wpdb;

  if($_SERVER['REQUEST_METHOD'] === 'POST') {
    $emailExists = checkIfExists('wp_persons', 'email', $_REQUEST['email']);

    if($emailExists) {
      echo '<div class="alert alert-danger" role="alert">Email already exists in database</div>';
      return listAll();
    }

    if($_REQUEST['name'] && $_REQUEST['email']) {
      $wpdb->update('wp_persons', [
        'name' => $_REQUEST['name'],
        'email' => $_REQUEST['email'],
      ], [
        'id' => $_POST['id']
      ]);
    }

    if(strlen($_REQUEST['number']) < 9) {
      echo '<div class="alert alert-danger" role="alert">Contact Number must be 9 digits long</div>';
      return listAll();
    }

    if($_REQUEST['contactId']) {
      $wpdb->update('wp_contacts', [
        'countryCode' => $_REQUEST['country'],
        'number' => $_REQUEST['number'],
      ], [
        'id' => $_POST['contactId'],
        'personId' => $_POST['id']
      ]);
    }

  }

  $sql = "SELECT id, name, email FROM wp_persons WHERE id = " . $id . " AND deletedAt is NULL";
  $person = $wpdb->get_row($sql);

  $sql = "SELECT id, countryCode, number FROM wp_contacts WHERE personId = " . $id;
  $contacts = $wpdb->get_results($sql);

  $countries = fetchAPI();

  echo '<div class="row d-flex flex-wrap align-items-center mt-3 mb-5">' . 
    '<div class="col-8">' .
      '<h1 class="mb-0">Showing data from: ' . $person->name . '</h1>' .
    '</div>' .
    '<div class="col-4">' .
      '<a href="/wp-admin/admin.php?page=persons_list" class="btn btn-warning ml-auto">Go back</a>' .
    '</div>' .
  '</div>';

  echo '<div class="row">' .
    '<div class="col-8">' . 
      '<form method="POST" action="">' . 
        '<input name="id" type="hidden" value="' . $person->id . '" readonly/>' .
        '<div class="form-group">' .
          '<label for="fld-name">Name</label>' . 
          '<input id="fld-name" name="name" type="text" value="' . $person->name . '" class="form-control" placeholder="Person name" minlength="5" required/>' .
        '</div>' .
        '<div class="form-group">' .
          '<label for="fld-email">Email address</label>' . 
          '<input id="fld-email" name="email" type="email" value="' . $person->email . '" class="form-control" placeholder="Person email" required/>' .
        '</div>' .
        '<input type="submit" value="Save" class="btn btn-primary">' .
      '</form>' .
    '</div>' .
  '</div>';

  echo '<div class="row d-flex flex-wrap align-items-center mt-5 mb-5">' . 
    '<div class="col-8">' .
      '<h2 class="mb-0">Showing contacts from: ' . $person->name . '</h2>' .
    '</div>' .
    '<div class="col-4">' .
      '<a href="/wp-admin/admin.php?page=persons_list&id=' . $person->id . '&action=addContact" class="btn btn-secondary">New contact</a>' .
    '</div>' .
  '</div>';

  echo '<div class="row w-100 align-items-center font-weight-bold text-center font-size-20">' . 
    '<p class="col-5">Country Code</p>' . 
    '<p class="col-5">Number</p>' . 
    '<p class="col-2">Actions</p>' . 
  '</div>';

  foreach($contacts as $key=>$contact) {
    echo '<div class="row">' .
      '<form method="POST" action="" class="col-12 mb-2">' . 
        '<input name="id" type="hidden" value="' . $person->id . '" readonly/>' .
        '<input name="contactId" type="hidden" value="' . $contact->id . '" readonly/>' .
        '<div class="row">' .
          '<div class="col-5">' .
            '<select id="fld-country" name="country" class="form-control">';

    foreach($countries as $key=>$country) {
      $selected = $country->callingCodes[0] == $contact->countryCode ? 'selected' : '';
      echo '<option value="' . $country->callingCodes[0] . '" ' . $selected . '>' . $country->name . ' (' . $country->callingCodes[0] . ')</option>';
    }

    echo '</select>' . '</div>';
    echo '<div class="col-5">' .
      '<input id="fld-number" name="number" type="text" value="' . $contact->number . '" class="form-control" placeholder="Contact number" minlength="9" maxlength="9" oninput="this.value = this.value.replace(/[^0-9.]/g, \'\').replace(/(\..*)\./g, \'$1\');" required/>' .
    '</div>' . 
    '<div class="col-2 d-flex justify-content-center align-items-center">' .
      '<input type="submit" value="Save" class="btn btn-primary mr-2">' .
      '<a href="/wp-admin/admin.php?page=persons_list&id=' . $person->id . '&contactId=' . $contact->id . '&action=deleteContact" class="btn btn-secondary">Delete</a>' .
    '</div>' . 
    '</div>' . '</form>' . '</div>';
  }
}

function delete($id) {
  global $wpdb;

  $wpdb->update('wp_persons', [
    'deletedAt' => date('Y-m-d H:i:s'),
  ], [
    'id' => $id
  ]);

  // header("Location: /wp-admin/admin.php?page=persons_list", TRUE, 301);
  listAll();
}

function fetchAPI() {
  $response = wp_remote_get('https://restcountries.com/v2/all?fields=name,callingCodes');

  if($response['response']['code'] !== 200) {
    echo 'Couldn\'t read data from remote API. Please, try again later.';
    return;
  }

  return json_decode($response['body']);
}

function addContact($id) {
  global $wpdb;

  if($_SERVER['REQUEST_METHOD'] === 'POST') {
    if(strlen($_REQUEST['number']) < 9) {
      echo '<div class="alert alert-danger" role="alert">Contact Number must be 9 digits long</div>';
      return listAll();
    }

    $contactExists = $wpdb->get_var("SELECT CONCAT(countryCode, ' ', number) AS contactNumber FROM wp_persons");

    if($contactExists) {
      echo '<div class="alert alert-danger" role="alert">Contact Number already exists in database</div>';
      return listAll();
    }

    $wpdb->insert('wp_contacts', [
      'countryCode' => $_REQUEST['country'],
      'number' => $_REQUEST['number'],
      'personId' => $_POST['id']
    ]);
  }

  $sql = "SELECT id, name, email FROM wp_persons WHERE id = " . $id . " AND deletedAt is NULL";
  $person = $wpdb->get_row($sql);
  $countries = fetchAPI();

  if(!$person) {
    echo '<div class="alert alert-danger" role="alert">Person not found in database</div>';
    return;
  }

  echo '<div class="row d-flex flex-wrap align-items-center mt-3 mb-5">' . 
    '<div class="col-8">' .
      '<h1 class="mb-0">Adding new Contact for: ' . $person->name . '</h1>' .
    '</div>' .
    '<div class="col-4">' .
      '<a href="/wp-admin/admin.php?page=persons_list" class="btn btn-warning ml-auto">Go back</a>' .
    '</div>' .
  '</div>';

  echo '<div class="row">' .
    '<div class="col-8">' . 
      '<form method="POST" action="">' . 
        '<input name="id" type="hidden" value="' . $person->id . '" readonly/>' .
        '<div class="form-group">' .
          '<label for="fld-country">Name</label>' . 
          '<select id="fld-country" name="country" class="form-control" required>';

  foreach($countries as $key=>$country) {
    echo '<option value="' . $country->callingCodes[0] . '">' . $country->name . ' (' . $country->callingCodes[0] . ')</option>';
  }

  echo '</select>' . '</div>' .
        '<div class="form-group">' .
          '<label for="fld-number">Number</label>' . 
          '<input id="fld-number" name="number" type="text" class="form-control" placeholder="Contact number" minlength="9" maxlength="9" oninput="this.value = this.value.replace(/[^0-9.]/g, \'\').replace(/(\..*)\./g, \'$1\');" required/>' .
        '</div>' .
        '<input type="submit" value="Save" class="btn btn-primary">' .
      '</form>' .
    '</div>' .
  '</div>';
}

function deleteContact($id) {
  global $wpdb;

  $wpdb->delete('wp_contacts', [
    'id' => $id
  ]);

  listAll();
}

function prefix_enqueue(){       
  wp_enqueue_script('js_bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js');
  wp_enqueue_style('css_bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css');
}
?>

